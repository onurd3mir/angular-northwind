import { CartItem } from "src/app/Model/cart-item";
import { CartAction, CartActionType } from "../actions/cart.action";

const initialState:Array<CartItem> = [];

export function CartReducer(
    state: CartItem[] = initialState,
    action: any
) {
    switch (action.type) {
        case CartActionType.CART_ADD:
            return [...state, action.payload];
        case CartActionType.CART_REMOVE:
            return state.filter(p=>p.product.productId !== action.payload.productId)
        default:
            return state;
    }
}


