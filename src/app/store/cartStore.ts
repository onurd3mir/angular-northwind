import { Injectable } from "@angular/core";
import { observable, action, computed } from "mobx-angular";
import { CartItem } from "../Model/cart-item";
import { Product } from "../Model/product";


@Injectable()
export class CartStore {

    @observable carts: CartItem[] = [];
    @observable cartTotal: number = 0;

    get(): CartItem[] {
        const data = sessionStorage.getItem('eshop.cart');
        if (data) return JSON.parse(data);
        return [];
    }

    set(cart: CartItem[]) {
        const data = JSON.stringify(cart);
        sessionStorage.setItem('eshop.cart', data);
    }

    @computed get getCartList():CartItem[]{
        this.carts=this.get();
        return this.carts;
    } 

    @action addCart(product: Product) {
        this.carts.push({ product: product, quantity: 1 });
        this.set(this.carts);
        this.getCartTotal();
    }

    @action removeCart(id: number) {
        let index = this.carts.findIndex(p => p.product.productId == id);
        this.carts.splice(index, 1);
        this.set(this.carts);
        this.getCartTotal();
    }

    @action getProductByCart(id: number) {
        return this.carts.findIndex(p => p.product.productId == id);
    }

    @action updateCart(index: number, quantity: number) {
        this.carts[index].quantity += quantity;
        this.set(this.carts);
        this.getCartTotal();
    }

    @action getCartTotal() {
        this.cartTotal=0;
        this.getCartList.forEach(c => {
            this.cartTotal += c.quantity * c.product.unitPrice;
        })
    }
}