import { Action } from "@ngrx/store";
import { CartItem } from "src/app/Model/cart-item"; 
import { Product } from "src/app/Model/product";

export enum CartActionType {
    CART_ADD = 'Add Cart',
    CART_REMOVE = 'Remove Cart'
}

export class AddCartAction implements Action{

    readonly type = CartActionType.CART_ADD;

    constructor(public payload:CartItem ) {}
}

export class RemoveCartAction implements Action{

    readonly type = CartActionType.CART_REMOVE;

    constructor(public payload:Product) {}
}

export type CartAction = AddCartAction | RemoveCartAction
