import { CategoryComponent } from './components/category/category.component';
import { ProductComponent } from './components/product/product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  // { path: "**", redirectTo:"products",pathMatch:"full" },
  //{ path: "products/category/:categoryId", component: ProductComponent },
  { path: "products", component: ProductComponent, },
  { path: "products/:categoryName-:categoryId", component: ProductComponent },
  { path: "product-add", component: ProductAddComponent, canActivate: [AuthGuard] },
  { path: "cart", component: CartComponent, },
  { path: "giris", component: LoginComponent },
  { path: "**", pathMatch: "full", component: ProductComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
