import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginModel } from '../Model/loginModel';
import { ResponseModel } from '../Model/responseModel';
import { TokenModel } from '../Model/tokenModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  apiURL = "http://localhost:61569/api"

  login(loginModel: LoginModel):Observable<ResponseModel<TokenModel>> {
    return this.httpClient.post<ResponseModel<TokenModel>>(this.apiURL + "/auth/login", loginModel);
  }

  register(){}

  get isAuthenticated(){
    if(localStorage.getItem('token')){
      return true;
    }
    else{
      return false;
    }
  }
}
