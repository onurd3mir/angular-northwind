import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { CartDbItem } from '../Model/cartDbItem';
import { Product } from '../Model/product';

@Injectable({
  providedIn: 'root'
})

export class CartService {

  carts: CartDbItem[] = [];

  constructor(private dbService: NgxIndexedDBService) { this.getAll(); }

  add(product: Product) {
    this.dbService
      .add('carts', {
        product: product,
        quantity: 1,
      })
      .subscribe((result) => {
        location.reload();
      });
  }

  remove(id: number) {
    this.dbService.deleteByKey('carts', id).subscribe((status) => {
      console.log('Deleted?:', status);
      location.reload();
    });
  }

  getAll() {
    this.carts = [];
    this.dbService.getAll('carts').subscribe((carts) => {
      carts.forEach((c: any) => {
        this.carts.push(new CartDbItem(c.id, c.product, c.quantity));
      })
    });
  }

  update(cartDbItem: CartDbItem, qty: number) {
    this.dbService
      .update('carts', {
        id: cartDbItem.id,
        product: cartDbItem.product,
        quantity: (cartDbItem.quantity + qty)
      })
      .subscribe((storeData) => {
        location.reload();
      });
  }

  getById(id: number) {
    return this.carts.find(p => p.product?.productId == id);
  }

  getCount(): number {
    return this.carts.length;
  }

  truncateTable(): void {
    this.dbService.clear('carts').subscribe((successDeleted) => {
      console.log('success? ', successDeleted);
    });
  }

  deleteDatabase(): void {
    this.dbService.deleteDatabase().subscribe((deleted) => {
      console.log('Database deleted successfully: ', deleted);
    });
  }

}
