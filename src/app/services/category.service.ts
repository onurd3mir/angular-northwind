import { Category } from './../Model/category';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from '../Model/responseModel';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  apiUrl = "http://localhost:61569/api";

  constructor(private httpClient: HttpClient) { }

  getCategories(): Observable<ResponseModel<Category[]>> {
    return this.httpClient.get<ResponseModel<Category[]>>(this.apiUrl + "/Categories/getall");
  }

}
