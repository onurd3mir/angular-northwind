import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from '../Model/responseModel';
import { Product } from '../Model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiuRL = "http://localhost:61569/api";

  constructor(private httpClient: HttpClient) { }

  getAllProducuts(): Observable<ResponseModel<Product[]>> {
    return this.httpClient.get<ResponseModel<Product[]>>(this.apiuRL + "/products/getall");
  }

  getProducutsByCategory(categoryId:number): Observable<ResponseModel<Product[]>> {
    return this.httpClient.get<ResponseModel<Product[]>>(this.apiuRL + "/Products/getlistbycategory?categoryId="+categoryId);
  }

  addProduct(product:Product){
    return this.httpClient.post(this.apiuRL+'/Products/add',product);
  }  

}










