import { ResponseBaseModel } from "./responseBaseModel";

export interface ResponseModel<T> extends ResponseBaseModel {
  data:T;
}
