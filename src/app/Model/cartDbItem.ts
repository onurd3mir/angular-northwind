import { Product } from "./product";

export class CartDbItem {

    constructor(_id:number,_product:Product,_quantity:number) {
        this.id=_id;
        this.product=_product;
        this.quantity=_quantity;
    }

    id: number;
    product?: Product;
    quantity: number;
}
