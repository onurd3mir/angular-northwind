import { CartItem } from "./cart-item";

export interface AppState {
    readonly carts: Array<CartItem>;
}
