import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'

import { ToastrModule } from 'ngx-toastr'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxIndexedDBModule,DBConfig  } from 'ngx-indexed-db';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StoreModule } from '@ngrx/store';
import { MobxAngularModule } from 'mobx-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './components/product/product.component'
import { CategoryComponent } from './components/category/category.component'
import { NavComponent } from './components/nav/nav.component'
import { CartComponent } from './components/cart/cart.component';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { LoginComponent } from './components/login/login.component';

import { AuthInterceptor } from './interceptors/auth.interceptor';

import { VatAddedPipe } from './pipes/vatAdded.pipe'
import { ProductNamePipe } from './pipes/productName.pipe'
import { ErrorNamePipe } from './pipes/errorName.pipe';

import { CartService } from 'src/app/services/cart.service';
import { CartStore } from './store/cartStore';
import { CartReducer } from './store/reducers/cart.reducer';



const dbConfig: DBConfig  = {
  name: 'Mydb',
  version: 1,
  objectStoresMeta: [{
    store: 'carts',
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name:'product', keypath:'product',options: { unique: false } },
      { name: 'quantity', keypath: 'quantity', options: { unique: false } }
    ]
  }]
};


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CategoryComponent,
    NavComponent,
    CartComponent,
    ProductAddComponent,
    LoginComponent,
    
  
    ProductNamePipe,
    VatAddedPipe,
    ErrorNamePipe,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass:"toast-bottom-right"
    }),
    NgxIndexedDBModule.forRoot(dbConfig),
    FontAwesomeModule,
    StoreModule.forRoot({
      carts:CartReducer
    }),
    MobxAngularModule 
  ],
  providers: [
    CartService,
    CartStore,
    {provide:HTTP_INTERCEPTORS,useClass:AuthInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
