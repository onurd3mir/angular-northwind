import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'errorName'
})
export class ErrorNamePipe implements PipeTransform {

  transform(value: any[], errorFilter: string): any[] {
    return value.filter(x => x.PropertyName == errorFilter);
  }
  
}
