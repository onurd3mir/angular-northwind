import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../Model/product';

@Pipe({
  name: 'productName'
})
export class ProductNamePipe implements PipeTransform {

  transform(value: Product[], filterText: string): Product[] {
    filterText = filterText ? filterText.toLocaleLowerCase() : ""
    return filterText.length>2
      ? value.filter((p: Product) => p.productName.toLocaleLowerCase().indexOf(filterText) !== -1)
      : value;
  }

}
