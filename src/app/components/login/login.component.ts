import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TokenModel } from 'src/app/Model/tokenModel';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toastrService: ToastrService,
    private route:Router) { }

  loginForm: FormGroup;
  tokenModel: TokenModel;

  ngOnInit() {
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(4)]]
    });
  }

  login() {
    if (this.loginForm.valid) {
      let model = Object.assign({}, this.loginForm.value);
      this.authService.login(model).subscribe(res => {
        this.tokenModel = res.data;
        localStorage.setItem('token', this.tokenModel.token);
        this.route.navigateByUrl('product');
        this.toastrService.info('Giriş Yapıldı');
      }, resError => {
        this.toastrService.error(resError.error);
      })
    }
  }

  get myForm() {
    return this.loginForm.controls;
  }

}
