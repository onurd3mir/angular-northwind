import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faShoppingCart, faUserLock, faSignInAlt, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';
import { CartStore } from 'src/app/store/cartStore';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  faShoppingCart = faShoppingCart;
  faUserLock = faUserLock;
  faSignInAlt = faSignInAlt;
  faSignOutAlt = faSignOutAlt;

  store: any

  constructor(private _store: CartStore, private authService: AuthService,private route:Router) { }

  ngOnInit() {
    this.store = this._store;
  }

  get isAuthenticated() {
    return this.authService.isAuthenticated;
  }

  logout(){
    localStorage.removeItem('token');
    this.route.navigateByUrl('giris');
  }

}
