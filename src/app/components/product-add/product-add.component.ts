import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css'],
  providers: [ProductService]
})
export class ProductAddComponent implements OnInit {

  constructor(private productService: ProductService,
    private formBuilder: FormBuilder, private toastrService: ToastrService) { }

  productAddForm: FormGroup;
  errorList: any = [];

  ngOnInit() {
    this.createProductAddForm();
  }

  createProductAddForm() {
    this.productAddForm = this.formBuilder.group({
      ProductName: ["", Validators.required],
      CategoryId: ["", Validators.required],
      QuantityPerUnit: [""],
      UnitPrice: ["", Validators.required],
      UnitsInStock: ["", Validators.required]
    });
  }

  add() {
    if (this.productAddForm.valid) {
      let productModel = Object.assign({}, this.productAddForm.value);
      console.log(productModel);
      this.productService.addProduct(productModel).subscribe(res => {
        console.log(res);
        this.productAddForm.reset();
        this.toastrService.success("Ürün Eklendi", "Başarılı");
      }, resError => {
        console.log(resError);
        if (resError.error.Errors.length > 0) {

          this.errorList = resError.error.Errors;

          for (let i = 0; i < resError.error.Errors.length; i++) {

            let errorCode = resError.error.Errors[i].ErrorCode.toString();
            let errorMessage = resError.error.Errors[i].ErrorMessage.toString();
            let errorPropertyName = resError.error.Errors[i].PropertyName.toString();

            //console.log(errorCode,errorMessage,errorPropertyName);
            //this.toastrService.error(resError.error.Errors[i].ErrorMessage, "Doğrulama Hatası");    
          }
        }
      });
    }
    else {
      this.toastrService.error("Formunuz Eksik")
    }
  }
}
