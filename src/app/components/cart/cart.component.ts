import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/Model/product';
import { CartStore } from 'src/app/store/cartStore';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private _store:CartStore ) { }

  store:any;

  ngOnInit() {
    this.store=this._store;
    this.store.getCartTotal();
  }

  removeCart(product:Product){
    this.store.removeCart(product.productId);
  }

}
