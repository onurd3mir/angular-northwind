import { Component, OnInit } from '@angular/core';
import { Category } from '../../Model/category';
import { CategoryService } from './../../services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [CategoryService]
})
export class CategoryComponent implements OnInit {

  constructor(private categoryService: CategoryService) { }

  dataLoaded = false;
  categories: Category[] = [];
  currentCategory: any;

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories().subscribe(res => {
      this.categories = res.data;
      this.dataLoaded = res.success;
    })
  }

  setCurrentCategory(category: Category) {
    this.currentCategory = category;
  }

  getCurrentCategoryClass(category: Category) {
    if (this.currentCategory && this.currentCategory.categoryId == category.categoryId) {
      return "list-group-item active";
    }
    return "list-group-item";
  }

  getAllProductClass() {
    this.currentCategory = {};
    return "list-group-item";
  }

  linkHelper(value:string){
    let newValue=value;
    newValue=newValue.replace("/","-");
    newValue=newValue.replace(" ","-");
    return newValue.toLocaleLowerCase();
  }


}
