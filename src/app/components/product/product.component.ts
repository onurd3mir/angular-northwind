import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Product } from '../../Model/product';
import { ProductService } from '../../services/product.service';
import { ToastrService } from 'ngx-toastr';
import { CartStore } from 'src/app/store/cartStore';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService]
})
export class ProductComponent implements OnInit {

  dataLoaded = false;
  products: Product[] = [];
  filterText = "";
  store:any;
 
  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private _store:CartStore) {}

  ngOnInit() {

    this.store=this._store;

    this.activatedRoute.params.subscribe(params => {
      this.dataLoaded = false;
      if (params["categoryId"]) {
        setTimeout(() => {
          this.getProductsByCategory(params["categoryId"]);
        }, 100);

      }
      else {
        setTimeout(() => {
          this.getProductAll();
        }, 100);

      }
    })

  }

  getProductAll() {
    this.productService.getAllProducuts().subscribe(res => {
      this.products = res.data;
      this.dataLoaded = res.success;
    });
  }

  getProductsByCategory(categoryId: number) {
    this.productService.getProducutsByCategory(categoryId).subscribe(res => {
      this.products = res.data;
      this.dataLoaded = true;
    });
  }

  addToCart(product: Product) {

    let cartExists = this.store.getProductByCart(product.productId);
    if(cartExists==-1){
      this.store.addCart(product);
    }
    else{
      this.store.updateCart(cartExists,1);
    }
    this.toastrService.success("Sepete Eklendi", product.productName);
  }

  
}
